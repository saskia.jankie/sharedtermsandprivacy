---
date: 2021-12-06
title: "Cookies"
---

## How FashionUnited uses cookies

A cookie is a small piece of text sent to your browser by a website you visit. It helps the website to remember information about your visit, like your preferred language and other settings. That can make your next visit easier and the site more useful to you. Cookies play an important role. Without them, using the web would be a much more frustrating experience.

We use cookies for many purposes. We use them, for example, to remember your safe search preferences, to make the ads you see more relevant to you, to count how many visitors we receive to a page, to help you sign up for our services and to protect your data.

You can see a list of the types of cookies used by FashionUnited and also find out how FashionUnited and our partners use cookies in advertising. Our [privacy policy](/privacy) explains how we protect your privacy in our use of cookies and other information.

### Store and / or open information on a device

Store and / or open information on a device,  like cookies and device identifiers presented to a user.

- [Cloudflare](https://www.cloudflare.com/privacypolicy/)
- Fastly
- Thinglink
- Albumizr
- Knightlab
- Google MyMaps
- LocalFocus
- Apester
- Line.do
- Countdown clock
- JSFiddle
- Codepen.io
- Infogram
- Typeform
- Genial.ly
- H5P

### Functional and statistical

- Google Analytics  

### Marketing

We use cookies and your profile for our own marketing goals. That way you are able to see ads for our products and services on other websites and apps.

- [Google Advertising Products](https://policies.google.com/technologies/partner-sites) 


### Social Media

When we integrate content from Facebook, Instagram or Soundcloud in our articles, cookies from these parties are placed to get to know more about your web surfing habits.

- Facebook
- Instagram
- SnapChat
- Soundcloud
- Spotify AB
- Twitter, Inc
- Vimeo
- Youtube  


### Advertising

Advertising keeps FashionUnited and many of the websites and services you use free of charge. We work hard to make sure that ads are safe, unobtrusive and as relevant as possible. For example, you won’t see pop-up ads on FashionUnited nor ads containing malware, ads for counterfeit goods or those that attempt to misuse your personal information.

FashionUnited uses Google Advertising Services to manage its advertising.

## How FashionUnited uses cookies in advertising

Cookies help to make advertising more effective. Without cookies, it’s harder for an advertiser to reach its audience, or to know how many ads were shown and how many clicks they received.

Many websites, such as news sites and blogs, partner with FashionUnited to show ads to their visitors. Working with our partners, we may use cookies for a number of purposes, such as to stop you from seeing the same ad over and over again, to detect and stop click fraud, and to show ads that are likely to be more relevant (such as ads based on websites you have visited).

We store a record of the ads we serve in our logs. These server logs typically include your web request, IP address, browser type, browser language, the date and time of your request, and one or more cookies that may uniquely identify your browser. We store this data for a number of reasons, the most important of which are to improve our services and to maintain the security of our systems. We anonymize this log data by removing part of the IP address (after 9 months) and cookie information (after 18 months).

## Our advertising cookies

To help our partners manage their advertising and websites, we offer many products, including AdSense, AdWords, FashionUnited Analytics, and a range of Google-branded services. When you visit a page that uses one of these products, either on one of FashionUnited’s sites or one of our partners’, various cookies may be sent to your browser.

These may be set from a few different domains, including al sorts of FashionUnited. domains, doubleclick.net, invitemedia.com, admeld.com, Googlesyndication.com, or Googleadservices.com. Some of our advertising products enable our partners to use other services in conjunction with ours (like an ad measurement and reporting service) and these services may send their own cookies to your browser. These cookies will be set from their domains.

See more detail about the types of cookie used by FashionUnited and our partners, and how we use them.

- Ad Manager/AdX  
- AdMob  
- AdSense  
- Microsoft Advertising  
- Hotjar  
- Mailchimp
- Google
- Aarki
- Adacado
- Adara Media
- ADEX
- Adform
- Adikteev
- AdLedge
- Adloox
- Adludio
- AdMaxim
- Admedo
- Admetrics
- Adobe Advertising Cloud
- AdTriba
- advanced STORE GmbH
- Adventori
- advolution.control
- Akamai
- Amazon
- Amobee
- Analights
- AppLovin Corp.
- AppNexus (Xandr) Inc
- Arrivalist
- Ask Locala (F.K.A Fusio by S4M)
- AudienceProject
- Aunica
- Avocet
- Bannerflow
- Basis Technologies
- Batch Media
- BDSK Handels GmbH & Co. KG
- Beeswax
- Betgenius
- Blismedia
- Bombora
- Booking.com
- C3 Metrics
- Cablato
- Celtra
- Cint
- Clinch
- Cloud Technologies
- Cloudflare
- Commanders Act
- comScore
- Crimtan
- Criteo
- CUBED
- DataXu
- Demandbase
- DENTSU
- Dentsu Aegis Network
- Digiseg
- DMA Institute
- DoubleVerify
- Dstillery
- Dynata
- EASYmedia
- eBay
- ebuilders
- Effinity
- emetriq
- Ensighten
- Epsilon
- Essens
- Evidon
- Exactag
- Exponential
- Facebook
- Flashtalking by Mediaocean
- Fractional Media
- Gemius
- GfK
- GP One
- GroupM
- gskinner
- Haensel AMS
- Havas Media France - DBi
- hurra.com
- IBM
- Ignition One
- Impact
- Improve Digital
- Index Exchange
- Innovid
- Integral Ad Science
- intelliAd
- IPONWEB
- Jivox
- Kantar
- Kochava
- LifeStreet
- Liftoff
- LiveRamp
- Localsensor
- LoopMe
- Lotame
- Macromill group
- Magnite
- MainADV
- Manage.com
- Marketing Science Consulting Group, Inc.
- MediaMath
- Meetrics
- MindTake Research
- Mobitrans
- Mobpro
- Moloco Ads
- MSI-ACI
- Nano Interactive
- Navegg
- Neodata Group
- NEORY GmbH
- Netflix
- Netquest
- Neural.ONE
- Neustar
- NextRoll, Inc.
- Nielsen
- numberly
- Objective Partners
- Omnicom Media Group
- On Device Research
- OneTag
- OpenX Technologies
- Optomaton
- Oracle Data Cloud
- OTTO
- PERMODO
- Pixalate
- Platform161
- Primis
- Protected Media
- Publicis Media
- PubMatic
- PulsePoint
- Quantcast
- Rackspace
- Rakuten Marketing
- Relay42
- Remerge
- Resolution Media
- Resonate
- RevJet
- Roq.ad
- RTB House
- Salesforce DMP
- Scenestealer
- Scoota
- Seenthis
- Semasio GmbH
- SFR
- Sift Media
- Simpli.fi
- Sizmek
- Smaato
- Smadex
- Smart
- Smartology
- Sojern
- Solocal
- Sovrn
- Spotad
- STRÖER SSP GmbH
- TabMo
- Taboola
- TACTIC™ Real-Time Marketing
- Teads
- TEEMO
- The Trade Desk
- Tradedoubler AB
- travel audience – An Amadeus Company
- Travel Data Collective
- TreSensa
- TripleLift
- TruEffect
- TrustArc
- UnrulyX
- usemax (Emego GmbH)
- Verve Group
- Videology
- Vimeo
- Virtual Minds
- Vodafone GmbH
- Waystack
- Weborama
- White Ops
- Widespace
- Wizaly
- Yahoo
- ZMS

### How you can control advertising cookies

You can use Ads Settings to manage the FashionUnited ads you see and opt out of interest-based ads. Even if you opt out of interest-based ads, you may still see ads based on factors such as your general location derived from your IP address, your browser type and recent, previous searches related to your current search.

You can also manage many companies’ cookies used for online advertising at the US-based aboutads.info choices page or the EU-based Your Online Choices.

Finally, you can manage cookies in your web browser.

## Other technologies used in advertising

FashionUnited’s advertising systems may use other technologies, including HTML5, for functions like display of interactive ad formats. We may use the IP address, for example, to identify your general location. We may also select advertising based on information about your computer or device, such as your device model, browser type, or sensors in your device like the accelerometer.

### Location

FashionUnited’s ad products may receive or infer information about your location from a variety of sources. For example, we may use the IP address to identify your general location; we may receive precise location from your mobile device; we may infer your location from your search queries; and websites or apps that you use may send information about your location to us. FashionUnited uses location information in its Google ads products to improve the relevance of the ads you see, to measure ad performance and to report anonymous statistics to advertisers.

### What determines the ads by FashionUnited that I see?

Many decisions are made to determine which ad you see.

In many cases ads are shown via Google Advertising Services. Sometimes the ad you see is based on your location. Your IP address is usually a good indication of your approximate location. So you might see an ad on the homepage of YouTube.com that promotes a forthcoming movie in your country.

Sometimes the ad you see is based on the context of a page. If you’re looking at a page of gardening tips, you might see ads for gardening equipment.

Sometimes the ad you see on a page is served by FashionUnited but selected by another company. For example, you might have registered with a newspaper website. From information you’ve given the newspaper, it can make decisions about which ads to show you, and it can use FashionUnited’s ad serving products to deliver those ads.

Why am I seeing ads by Google for products I’ve viewed?
You may see ads for products you previously viewed through what's known as remarketing. Let’s suppose you visit a website that sells jeans, but you don’t buy those jeans on your first visit. The website owner might want to encourage you to return and complete your purchase. Google offers services that let website operators target their ads to people who visited their pages. Read the [Google Privacy & Terms site](https://policies.google.com/technologies/partner-sites) for details about the Google services.

When you visit another site that works with Google, which may have nothing to do with jeans, you might see an ad for those jeans. That’s because your browser sends Google the same cookie. In turn, we may use that cookie to serve you an ad that could encourage you to buy those jeans.

Read the [Privacy Policy](/privacy).
